﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace laboratorium3 {
    // test
    public partial class Form1 : Form {

        private WebPage _webPage;
        private Mail _email;
        private Logs _log;

        public Form1() {
            _log = new Logs();
            InitializeComponent();
            _log.write("Log","GUI loaded.");
        }

        private void executeButton_Click(object sender, EventArgs e) {
            _log.write("Log", "Execute button clicked.");
            if (urlBox.Text.Length > 0 && textBox.Text.Length > 0 && emailBox.Text.Length > 0) {
                try {
                    _webPage = new WebPage(urlBox.Text);
                    _log.write("Log", "Page downloaded.");
                    _email = new Mail();
                    var imageUrl = _webPage.getImageUrl(textBox.Text);
                    _log.write("Log", "Image url obtained.");
                    _email.sendMail(emailBox.Text, textBox.Text, imageUrl);
                    _log.write("Log", "Email sent.");
                    MessageBox.Show("Email wysłany.", "Komunikat", MessageBoxButtons.OK);
                } catch (Exception ex) {
                    _log.write("Log", ex.Message);
                    MessageBox.Show(this, "Błąd: \n" + ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } else {
                _log.write("Warning", "Not all text boxes filled.");
                MessageBox.Show("Uzupełnij wszystkie pola!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}