﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorium3 {
    class Logs {
        private string _logFileName;
        public Logs() {
            _logFileName = "laboratorium3.log";
        }

        public void write(string type, string message) {
            FileStream fs;

            if (!File.Exists(_logFileName)) {
                fs = File.Create(_logFileName);
                fs.Close();
            }

            StreamWriter sw = new StreamWriter(_logFileName, true);
            sw.WriteLine(string.Format("{0} - {1} - {2}",
                                  DateTime.Now.ToString("HH:mm:ss dd-MM-yyyy"),
                                  type,
                                  message));
            sw.Close();
        }
    }
}
