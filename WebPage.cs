﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorium3 {
    class WebPage {
        private string _url;

        public WebPage(string url) {
            _url = url;
        }

        public string getImageUrl(string text) {
            string url = "";
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument document = web.Load(_url);
            var nodes = document.DocumentNode.Descendants("img");
            string src;
            string alt;

            foreach (var node in nodes) {
                src = node.GetAttributeValue("src", "");
                alt = node.GetAttributeValue("alt", "");
                Console.WriteLine(src);
                Console.WriteLine(alt);

                if(alt.Contains(text)) {
                    url = src;
                    Console.WriteLine(url);
                    break;
                } 
            }
            return url;
        }
    }
}
