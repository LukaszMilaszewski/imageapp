﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using Mono.Web;

namespace laboratorium3 {
    class Mail {
        private string _login;
        private string _password;
        private string _who;

        public Mail() {
            _login = "laboratoriumdotnet1234@gmail.com";
            _password = "laboratorium1234";
            _who = "Lukasz i Dawid";
        }

        public void sendMail(string emailAddress, string subject, string imageUrl) {
            subject = parseSubject(subject);
            imageUrl = parseImageUrl(imageUrl);

            MailMessage message = new MailMessage();
            ContentType mimeType = new System.Net.Mime.ContentType("text/html");
            string body = HttpUtility.HtmlDecode(imageUrl);
            AlternateView alternate = AlternateView.CreateAlternateViewFromString(body, mimeType);
            message.AlternateViews.Add(alternate);
            message.From = new MailAddress(_login, _who);
            message.To.Add(new MailAddress(emailAddress));
            message.Subject = subject;
            message.Body = body;

            var smtp = new SmtpClient("smtp.gmail.com");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(_login, _password);
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Send(message);
        }

        public string parseSubject(string subject) {
            return "imageApp: " + subject;
        }

        public string parseImageUrl(string imageUrl) {
            return "<img alt=\"Brak Obrazka\" src=\"" + imageUrl + "\" >";
        }
    }
}
